package com.solutions.ui;

public enum State {
    SHOW_LIST,
    SELECT_ITEM,
    EDIT_ITEM_DESCRIPTION,
    EDIT_ITEM_DEADLINE,
    EDIT_ITEM_PRIORITY,
    EDIT_ITEM_IS_DONE,
    EXIT
}
