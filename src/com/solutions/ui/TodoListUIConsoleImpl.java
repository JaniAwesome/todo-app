package com.solutions.ui;

import com.solutions.Localization;
import com.solutions.TodoListService;
import com.solutions.entities.Priority;
import com.solutions.usecases.*;
import com.solutions.utils.DateUtils;
import com.solutions.utils.IntegerUtils;
import com.solutions.viewmodels.TodoItemViewModel;
import com.solutions.viewmodels.TodoItemViewModelImpl;

import java.util.List;
import java.util.Date;

public class TodoListUIConsoleImpl implements TodoListUI {

    private String mLineSeparator;
    private TodoListService mService;
    private Localization mLocalization;
    private List<TodoItemViewModel> mItems;
    private TodoItemViewModel mSelectedItem;
    private State mState = State.SHOW_LIST;
    private String mUserInput = null;
    private String mMessage = "";

    public TodoListUIConsoleImpl(TodoListService service, Localization localization) {
        mLineSeparator = System.getProperty("line.separator");
        mService = service;
        mLocalization = localization;
        mItems = mService.getItems(new GetItemsRequestImpl());
    }

    @Override
    public void render() {
        clearScreen();
        printHeader();
        switch (mState) {
            case SHOW_LIST:
                printList();
                printActionsMenu();
                break;
            case SELECT_ITEM:
                printItemDetails();
                printActionsMenu();
                break;
            case EDIT_ITEM_DESCRIPTION:
            case EDIT_ITEM_DEADLINE:
            case EDIT_ITEM_PRIORITY:
            case EDIT_ITEM_IS_DONE:
                printItemDetails();
                printActionsMenu();
                break;
            default:
                // unkown state no op
                break;
        }
        printMessage();
        mMessage = "";
    }

    @Override
    public void handleInput(String input) {
        mUserInput = sanitizeUserInput(input);
        switch (mState) {
            case SHOW_LIST:
                mUserInput = mUserInput.toLowerCase();
                if (mItems.size() > 0) {
                    Integer parsed = IntegerUtils.tryParseInt(mUserInput);
                    if (parsed != null) {
                        for (TodoItemViewModel model : mItems) {
                            if (model.getIndex() == parsed) {
                                mSelectedItem = model;
                                mState = State.SELECT_ITEM;
                                return;
                            }
                        }
                    }
                }
                if ("a".equals(mUserInput)) {
                    mSelectedItem = new TodoItemViewModelImpl(0, "");
                    mState = State.EDIT_ITEM_DESCRIPTION;
                    return;
                } else if ("e".equals(mUserInput)) {
                    mState = State.EXIT;
                    return;
                }
                break;
            case SELECT_ITEM:
                mUserInput = mUserInput.toLowerCase();
                if (mSelectedItem == null) {
                    mState = State.SHOW_LIST;
                    return;
                }
                if ("e".equals(mUserInput)) {
                    mState = State.EDIT_ITEM_DESCRIPTION;
                    return;
                } else if ("r".equals(mUserInput)) {
                    RemoveItemRequest request = new RemoveItemRequestImpl(mSelectedItem.getKey());
                    mService.removeItem(request);
                    mItems = mService.getItems(new GetItemsRequestImpl());
                    mSelectedItem = null;
                    mMessage = mLocalization.getString("item_removed");
                    mState = State.SHOW_LIST;
                    return;
                } else if ("m".equals(mUserInput)) {
                    mSelectedItem.setIsDone(true);
                    SaveItemRequest request = new SaveItemRequestImpl(mSelectedItem);
                    mService.saveItem(request);
                    mItems = mService.getItems(new GetItemsRequestImpl());
                    mSelectedItem = null;
                    mMessage = mLocalization.getString("item_marked_done");
                    mState = State.SHOW_LIST;
                    return;
                } else if ("c".equals(mUserInput)) {
                    mSelectedItem = null;
                    mState = State.SHOW_LIST;
                    return;
                }
                break;
            case EDIT_ITEM_DESCRIPTION:
                if (mSelectedItem == null) {
                    mState = State.SHOW_LIST;
                    return;
                }
                if ("c".equals(mUserInput.toLowerCase())) {
                    mSelectedItem = null;
                    mState = State.SHOW_LIST;
                    return;
                } else {
                    mSelectedItem.setDescription(mUserInput);
                    mState = State.EDIT_ITEM_DEADLINE;
                    return;
                }
            case EDIT_ITEM_DEADLINE:
                if (mSelectedItem == null) {
                    mState = State.SHOW_LIST;
                    return;
                }
                if ("c".equals(mUserInput.toLowerCase())) {
                    mSelectedItem = null;
                    mState = State.SHOW_LIST;
                    return;
                } else {
                    Date date = DateUtils.parseDateTime(mUserInput, mLocalization.getDateFormat());
                    if (date != null) {
                        mSelectedItem.setDeadline(mUserInput);
                        mState = State.EDIT_ITEM_PRIORITY;
                        return;
                    }
                }
                break;
            case EDIT_ITEM_PRIORITY:
                if (mSelectedItem == null) {
                    mState = State.SHOW_LIST;
                    return;
                }
                if ("c".equals(mUserInput.toLowerCase())) {
                    mSelectedItem = null;
                    mState = State.SHOW_LIST;
                    return;
                } else {
                    Priority priority = null;
                    if ("1".equals(mUserInput)) {
                        priority = Priority.LOW;
                    } else if ("2".equals(mUserInput)) {
                        priority = Priority.HIGH;
                    }
                    if (priority != null) {
                        mSelectedItem.setPriority(priority);
                        SaveItemRequest request = new SaveItemRequestImpl(mSelectedItem);
                        mService.saveItem(request);
                        mItems = mService.getItems(new GetItemsRequestImpl());
                        mSelectedItem = null;
                        mMessage = mLocalization.getString("item_saved");
                        mState = State.SHOW_LIST;
                        return;
                    }
                }
                break;
            case EXIT:
                System.out.println("Goodbye!");
                return;
            default:
                // unkown state no op
                break;
        }
        mMessage = mLocalization.getString("invalid_input");
    }

    @Override
    public State getState() {
        return mState;
    }

    private void clearScreen() {
        System.out.print("\f");
    }

    private String sanitizeUserInput(String input) {
        // TODO: escape illegal characters
        if (input == null) {
            input = "";
        }
        return input.trim();
    }

    private void printHeader() {
        String appName = mLocalization.getString("app_name");
        System.out.println(appName);
    }

    private void printList() {
        String hdrFormat = "| %-3s | %-15s | %-12s | %-12s | %-5s |%s";
        String leftAlignFormat = "| %-3d | %-15s | %-12s | %-12s | %-5s |%s";
        StringBuilder sb = new StringBuilder();
        String hdrIndex = mLocalization.getString("index");
        String hdrDescription = mLocalization.getString("description");
        String hdrTime = mLocalization.getString("deadline");
        String hdrPriority = mLocalization.getString("priority");
        String hdrDone = mLocalization.getString("done");
        sb.append("+-------------------------------------------------------------+" + mLineSeparator);
        sb.append(String.format(hdrFormat,
                hdrIndex, hdrDescription, hdrTime, hdrPriority, hdrDone, mLineSeparator));
        sb.append("+-------------------------------------------------------------+" + mLineSeparator);
        for (TodoItemViewModel item : mItems) {
            String priority = mLocalization.getString(item.getPriority() == Priority.LOW ?
                    "priority_low" : "priority_high");
            String done = item.getIsDone() ? "*" : "";
            sb.append(String.format(leftAlignFormat,
                    item.getIndex(),
                    item.getDescription(),
                    item.getDeadline(),
                    priority,
                    done,
                    mLineSeparator));
        }
        sb.append("+-------------------------------------------------------------+" + mLineSeparator);
        System.out.println(sb.toString());
    }

    private void printActionsMenu() {

        System.out.println("************************************************");
        StringBuilder sb = new StringBuilder();
        switch (mState) {
            case SHOW_LIST:
                if (mItems.size() > 0) {
                    sb.append(String.format(mLocalization.getString("action_select_item"), 1, mItems.size()) + ", ");
                }
                sb.append(mLocalization.getString("action_add_item") + ", ");
                sb.append(mLocalization.getString("action_exit"));
                break;
            case SELECT_ITEM:
                sb.append(mLocalization.getString("action_edit") + ", ");
                sb.append(mLocalization.getString("action_remove") + ", ");
                sb.append(mLocalization.getString("action_mark_done") + ", ");
                sb.append(mLocalization.getString("action_cancel"));
                break;
            case EDIT_ITEM_DESCRIPTION:
                sb.append(mLocalization.getString("input_description") + ", ");
                sb.append(mLocalization.getString("action_cancel"));
                break;
            case EDIT_ITEM_DEADLINE:
                sb.append(mLocalization.getString("input_deadline") + ", ");
                sb.append(mLocalization.getString("action_cancel"));
                break;
            case EDIT_ITEM_PRIORITY:
                sb.append(mLocalization.getString("input_priority") + ", ");
                sb.append(mLocalization.getString("action_cancel"));
                break;
            default:
                // unkown state no op
                break;
        }
        System.out.println(sb.toString());
        System.out.println("************************************************");
        System.out.println(mLocalization.getString("input") + " ");
    }

    private void printMessage() {
        System.out.println(mMessage);
    }

    private void printItemDetails() {
        if (mSelectedItem != null && mSelectedItem.getKey() > 0) {
            System.out.println(mLocalization.getString("selected_item"));
            String hdrFormat = "| %-3s | %-15s | %-12s | %-12s | %-5s |%s";
            String leftAlignFormat = "| %-3d | %-15s | %-12s | %-12s | %-5s |%s";
            StringBuilder sb = new StringBuilder();
            String hdrIndex = mLocalization.getString("index");
            String hdrDescription = mLocalization.getString("description");
            String hdrTime = mLocalization.getString("deadline");
            String hdrPriority = mLocalization.getString("priority");
            String hdrDone = mLocalization.getString("done");
            sb.append("+-------------------------------------------------------------+" + mLineSeparator);
            sb.append(String.format(hdrFormat,
                    hdrIndex, hdrDescription, hdrTime, hdrPriority, hdrDone, mLineSeparator));
            sb.append("+-------------------------------------------------------------+" + mLineSeparator);
            String priority = mLocalization.getString(mSelectedItem.getPriority() == Priority.LOW ?
                        "priority_low" : "priority_high");
            String done = mSelectedItem.getIsDone() ? "*" : "";
            sb.append(String.format(leftAlignFormat,
                    mSelectedItem.getIndex(),
                    mSelectedItem.getDescription(),
                    mSelectedItem.getDeadline(),
                    priority,
                    done,
                    mLineSeparator));

            sb.append("+-------------------------------------------------------------+" + mLineSeparator);
            System.out.println(sb.toString());
        } else {
            System.out.println(mLocalization.getString("new_item"));
        }
    }
}
