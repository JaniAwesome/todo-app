package com.solutions.ui;

public interface TodoListUI {

    void render();

    void handleInput(String input);

    State getState();
}
