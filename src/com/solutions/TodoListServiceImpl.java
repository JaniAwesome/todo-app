package com.solutions;

import com.solutions.presenters.GetItemsPresenterImpl;
import com.solutions.presenters.RemoveItemPresenterImpl;
import com.solutions.presenters.SaveItemPresenterImpl;
import com.solutions.repositories.TodoItemRepository;
import com.solutions.usecases.*;
import com.solutions.viewmodels.TodoItemViewModel;
import com.solutions.viewmodels.TodoItemViewModelImpl;

import java.util.List;

public class TodoListServiceImpl implements TodoListService {

    private Localization mLocalization;
    // repositories
    private TodoItemRepository mRepository;
    // use cases
    private UseCase<GetItemsRequest, GetItemsResponse> mGetItemsUseCase;
    private UseCase<SaveItemRequest, SaveItemResponse> mSaveItemUseCase;
    private UseCase<RemoveItemRequest, RemoveItemResponse> mRemoveItemUseCase;
    // presenters
    private GetItemsPresenterImpl mGetItemsPresenter;
    private SaveItemPresenterImpl mSaveItemPresenter;
    private RemoveItemPresenterImpl mRemoveItemPresenter;

    public TodoListServiceImpl(TodoItemRepository repository, Localization localization) {
        mLocalization = localization;
        mRepository = repository;
        mGetItemsUseCase = new GetItemsUseCaseImpl(mRepository);
        mSaveItemUseCase = new SaveItemUseCaseImpl(mRepository, mLocalization);
        mRemoveItemUseCase = new RemoveItemUseCaseImpl(mRepository);
        mGetItemsPresenter = new GetItemsPresenterImpl(localization);
        mSaveItemPresenter = new SaveItemPresenterImpl(mLocalization);
        mRemoveItemPresenter = new RemoveItemPresenterImpl();
    }

    @Override
    public List<TodoItemViewModel> getItems(GetItemsRequest request) {
        mGetItemsUseCase.handle(request, mGetItemsPresenter);
        return mGetItemsPresenter.getItems();
    }

    @Override
    public TodoItemViewModel saveItem(SaveItemRequest request) {
        mSaveItemUseCase.handle(request, mSaveItemPresenter);
        return mSaveItemPresenter.getItem();
    }

    @Override
    public void removeItem(RemoveItemRequest request) {
        mRemoveItemUseCase.handle(request, mRemoveItemPresenter);
    }
}
