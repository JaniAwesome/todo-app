package com.solutions.presenters;

import com.solutions.Localization;
import com.solutions.entities.TodoItemEntity;
import com.solutions.usecases.OutputPort;
import com.solutions.usecases.SaveItemResponse;
import com.solutions.viewmodels.TodoItemViewModel;
import com.solutions.viewmodels.TodoItemViewModelImpl;

import java.text.SimpleDateFormat;

public class SaveItemPresenterImpl implements OutputPort<SaveItemResponse> {

    private Localization mLocalization;
    private TodoItemViewModel mItem;

    public SaveItemPresenterImpl(Localization localization) {
        mLocalization = localization;
    }

    public TodoItemViewModel getItem() {
        return mItem;
    }

    @Override
    public void handle(SaveItemResponse response) {
        SimpleDateFormat dateFormat = mLocalization.getDateFormat();
        TodoItemEntity entity = response.getItem();
        mItem = new TodoItemViewModelImpl(entity.getKey(), entity.getDescription());
        mItem.setPriority(entity.getPriority());
        mItem.setDeadline(dateFormat.format(entity.getDeadline()));
        mItem.setIsDone(entity.getIsDone());
    }
}
