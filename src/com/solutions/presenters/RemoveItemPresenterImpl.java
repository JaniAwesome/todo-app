package com.solutions.presenters;

import com.solutions.usecases.OutputPort;
import com.solutions.usecases.RemoveItemResponse;

public class RemoveItemPresenterImpl implements OutputPort<RemoveItemResponse> {

    @Override
    public void handle(RemoveItemResponse removeItemResponse) {
        // no op
    }
}
