package com.solutions.presenters;

import com.solutions.Localization;
import com.solutions.entities.TodoItemEntity;
import com.solutions.usecases.GetItemsResponse;
import com.solutions.usecases.OutputPort;
import com.solutions.viewmodels.TodoItemViewModel;
import com.solutions.viewmodels.TodoItemViewModelImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class GetItemsPresenterImpl implements OutputPort<GetItemsResponse> {

    private Localization mLocalization;
    private List<TodoItemViewModel> mItems;

    public GetItemsPresenterImpl(Localization localization) {
        mLocalization = localization;
    }

    public List<TodoItemViewModel> getItems() {
        return mItems;
    }

    @Override
    public void handle(GetItemsResponse response) {
        SimpleDateFormat dateFormat = mLocalization.getDateFormat();
        mItems = new ArrayList<>();
        int index = 1;
        for (TodoItemEntity entity : response.getItems()) {
            TodoItemViewModel model = new TodoItemViewModelImpl(entity.getKey(), entity.getDescription());
            model.setIndex(index);
            model.setPriority(entity.getPriority());
            model.setDeadline(dateFormat.format(entity.getDeadline()));
            model.setIsDone(entity.getIsDone());
            index++;
            mItems.add(model);
        }
    }
}
