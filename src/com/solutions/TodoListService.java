package com.solutions;

import com.solutions.usecases.GetItemsRequest;
import com.solutions.usecases.RemoveItemRequest;
import com.solutions.usecases.SaveItemRequest;
import com.solutions.viewmodels.TodoItemViewModel;
import com.solutions.viewmodels.TodoItemViewModelImpl;

import java.util.List;

public interface TodoListService {

    List<TodoItemViewModel> getItems(GetItemsRequest request);

    TodoItemViewModel saveItem(SaveItemRequest request);

    void removeItem(RemoveItemRequest request);
}
