package com.solutions.viewmodels;

import com.solutions.entities.Priority;

public interface TodoItemViewModel {
    void setKey(int key);

    int getKey();

    void setIndex(int index);

    int getIndex();

    void setDescription(String description);

    String getDescription();

    void setPriority(Priority priority);

    Priority getPriority();

    void setDeadline(String date);

    String getDeadline();

    void setIsDone(boolean isDone);

    boolean getIsDone();
}
