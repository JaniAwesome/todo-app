package com.solutions.viewmodels;

import com.solutions.entities.Priority;

public class TodoItemViewModelImpl implements TodoItemViewModel {
    private int mKey;
    private int mIndex;
    private String mDescription;
    private Priority mPriority = Priority.LOW;
    private String mDeadline = null;
    private boolean mIsDone = false;

    public TodoItemViewModelImpl(int key, String description) {
        mKey = key;
        mDescription = description;
    }

    @Override
    public void setKey(int key) {
        mKey = key;
    }

    @Override
    public int getKey() {
        return mKey;
    }

    @Override
    public void setIndex(int index) {
        mIndex = index;
    }

    @Override
    public int getIndex() {
        return mIndex;
    }

    @Override
    public void setDescription(String description) {
        mDescription = description;
    }

    @Override
    public String getDescription() {
        return mDescription;
    }

    @Override
    public void setPriority(Priority priority) {
        mPriority = priority;
    }

    @Override
    public Priority getPriority() {
        return mPriority;
    }

    @Override
    public void setDeadline(String date) {
        mDeadline = date;
    }

    @Override
    public String getDeadline() {
        return mDeadline;
    }

    @Override
    public void setIsDone(boolean isDone) {
        mIsDone = isDone;
    }

    @Override
    public boolean getIsDone() {
        return mIsDone;
    }
}
