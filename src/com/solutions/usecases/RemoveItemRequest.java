package com.solutions.usecases;

public interface RemoveItemRequest {

    int getKey();
}
