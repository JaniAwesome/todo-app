package com.solutions.usecases;

import com.solutions.entities.TodoItemEntity;

public interface SaveItemResponse {

    TodoItemEntity getItem();
}
