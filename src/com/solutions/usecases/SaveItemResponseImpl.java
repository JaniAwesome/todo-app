package com.solutions.usecases;

import com.solutions.entities.TodoItemEntity;

public class SaveItemResponseImpl implements SaveItemResponse {

    private TodoItemEntity mItem;

    public SaveItemResponseImpl(TodoItemEntity item) {
        mItem = item;
    }

    @Override
    public TodoItemEntity getItem() {
        return mItem;
    }
}
