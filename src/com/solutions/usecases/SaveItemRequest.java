package com.solutions.usecases;

import com.solutions.viewmodels.TodoItemViewModel;

public interface SaveItemRequest {

    TodoItemViewModel getItem();
}
