package com.solutions.usecases;

import com.solutions.entities.TodoItemEntity;

import java.util.List;

public interface GetItemsResponse {

    List<TodoItemEntity> getItems();
}
