package com.solutions.usecases;

import com.solutions.Localization;
import com.solutions.entities.TodoItemEntity;
import com.solutions.entities.TodoItemEntityImpl;
import com.solutions.repositories.TodoItemRepository;
import com.solutions.utils.DateUtils;
import com.solutions.viewmodels.TodoItemViewModel;

import java.util.Date;

public class SaveItemUseCaseImpl implements UseCase<SaveItemRequest, SaveItemResponse> {

    private Localization mLocalization;
    private TodoItemRepository mRepository;

    public SaveItemUseCaseImpl(TodoItemRepository repository, Localization localization) {
        mRepository = repository;
        mLocalization = localization;
    }

    @Override
    public boolean handle(SaveItemRequest request, OutputPort<SaveItemResponse> outputPort) {
        TodoItemViewModel item = request.getItem();
        if (item == null) {
            return false;
        }
        Date deadline = DateUtils.parseDateTime(item.getDeadline(), mLocalization.getDateFormat());
        if (deadline == null) {
            return false;
        }
        TodoItemEntity entity = new TodoItemEntityImpl(item.getKey(), item.getDescription());
        entity.setPriority(item.getPriority());
        entity.setDeadline(deadline);
        entity.setIsDone(item.getIsDone());
        TodoItemEntity savedItem = mRepository.saveItem(entity);
        SaveItemResponse response = new SaveItemResponseImpl(savedItem);
        outputPort.handle(response);
        return true;
    }
}

