package com.solutions.usecases;

public interface OutputPort<TUseCaseResponse> {


    void handle(TUseCaseResponse response);
}
