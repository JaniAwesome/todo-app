package com.solutions.usecases;

public class RemoveItemRequestImpl implements RemoveItemRequest {

    private int mKey;

    public RemoveItemRequestImpl(int key) {
        mKey = key;
    }

    @Override
    public int getKey() {
        return mKey;
    }
}
