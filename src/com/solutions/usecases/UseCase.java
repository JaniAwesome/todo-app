package com.solutions.usecases;

public interface UseCase<TUseCaseRequest, TUseCaseResponse> {

    /**
     * Performs use case actions according to the request message and calls the output port with a response
     *
     * @param request       Request message
     * @param outputPort    Output port
     *
     * @return          <code>true</code> operation was successful
     *                  <code>false</code> operation failed
     */
    boolean handle(TUseCaseRequest request, OutputPort<TUseCaseResponse> outputPort);
}
