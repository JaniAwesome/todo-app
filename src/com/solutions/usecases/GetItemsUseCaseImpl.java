package com.solutions.usecases;

import com.solutions.entities.TodoItemEntity;
import com.solutions.repositories.TodoItemRepository;

import java.util.List;

public class GetItemsUseCaseImpl implements UseCase<GetItemsRequest, GetItemsResponse> {

    private TodoItemRepository mRepository;

    public GetItemsUseCaseImpl(TodoItemRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public boolean handle(GetItemsRequest request, OutputPort<GetItemsResponse> outputPort) {
        List<TodoItemEntity> items = mRepository.getAllItems();
        GetItemsResponse response = new GetItemsResponseImpl(items);
        outputPort.handle(response);
        return true;
    }
}
