package com.solutions.usecases;

import com.solutions.repositories.TodoItemRepository;

public class RemoveItemUseCaseImpl implements UseCase<RemoveItemRequest, RemoveItemResponse> {

    private TodoItemRepository mRepository;

    public RemoveItemUseCaseImpl(TodoItemRepository repository) {
        mRepository = repository;
    }

    @Override
    public boolean handle(RemoveItemRequest request, OutputPort<RemoveItemResponse> outputPort) {
        mRepository.removeItem(request.getKey());
        RemoveItemResponse response = new RemoveItemResponseImpl();
        outputPort.handle(response);
        return true;
    }
}
