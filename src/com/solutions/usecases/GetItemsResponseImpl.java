package com.solutions.usecases;

import com.solutions.entities.TodoItemEntity;

import java.util.List;

public class GetItemsResponseImpl implements GetItemsResponse {

    private List<TodoItemEntity> mItems;

    public GetItemsResponseImpl(List<TodoItemEntity> items) {
        this.mItems = items;
    }

    @Override
    public List<TodoItemEntity> getItems() {
        return this.mItems;
    }
}
