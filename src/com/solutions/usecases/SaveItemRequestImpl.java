package com.solutions.usecases;

import com.solutions.viewmodels.TodoItemViewModel;

public class SaveItemRequestImpl implements SaveItemRequest {

    private TodoItemViewModel mItem;

    public SaveItemRequestImpl(TodoItemViewModel item) {
        mItem = item;
    }

    @Override
    public TodoItemViewModel getItem() {
        return mItem;
    }
}
