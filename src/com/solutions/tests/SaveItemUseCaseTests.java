package com.solutions.tests;

import com.solutions.Constants;
import com.solutions.Localization;
import com.solutions.LocalizationImpl;
import com.solutions.entities.TodoItemEntity;
import com.solutions.usecases.*;
import com.solutions.utils.DateUtils;
import com.solutions.viewmodels.TodoItemViewModel;
import com.solutions.viewmodels.TodoItemViewModelImpl;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;

public class SaveItemUseCaseTests {

    // TODO: create mock
    private Localization mLocalization;
    private MockTodoItemRepositoryImpl mTodoItemRepository;
    private UseCase<SaveItemRequest, SaveItemResponse> mSaveItemUseCase;

    @Before
    public void setup() {
        mLocalization = new LocalizationImpl(Constants.LANG_CODE_EN);
        mTodoItemRepository = new MockTodoItemRepositoryImpl();
        mSaveItemUseCase = new SaveItemUseCaseImpl(mTodoItemRepository, mLocalization);
    }

    @Test
    public void saveNewItem() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TodoItemViewModel item = new TodoItemViewModelImpl(0, "new item");
        item.setDeadline("2019-02-10");
        final SaveItemRequest request = new SaveItemRequestImpl(item);
        final MockSaveItemOutputPortImpl presenter = new MockSaveItemOutputPortImpl();
        assertEquals(null, presenter.getResponse());
        final boolean result = mSaveItemUseCase.handle(request, presenter);
        // Todo: assert result
        TodoItemEntity entity = presenter.getResponse().getItem();
        assertEquals(item.getDescription(), entity.getDescription());
    }

    private class MockSaveItemOutputPortImpl implements OutputPort<SaveItemResponse> {

        private SaveItemResponse mResponse;

        public SaveItemResponse getResponse() {
            return mResponse;
        }

        @Override
        public void handle(SaveItemResponse response) {
            mResponse = response;
        }
    }
}



