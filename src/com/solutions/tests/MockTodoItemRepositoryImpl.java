package com.solutions.tests;

import com.solutions.entities.Priority;
import com.solutions.entities.TodoItemEntity;
import com.solutions.entities.TodoItemEntityImpl;
import com.solutions.repositories.TodoItemRepository;
import com.solutions.utils.DateUtils;

import java.text.SimpleDateFormat;
import java.util.*;

public class MockTodoItemRepositoryImpl implements TodoItemRepository {

    private HashMap<Integer, TodoItemEntity> mTestItems = new HashMap<>();

    public MockTodoItemRepositoryImpl() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TodoItemEntity todo1 = new TodoItemEntityImpl(1, "First item");
        todo1.setPriority(Priority.HIGH);
        todo1.setDeadline(DateUtils.parseDateTime("2019-01-01 08:00:00", format));
        todo1.setIsDone(true);
        mTestItems.put(todo1.getKey(), todo1);
        TodoItemEntity todo2 = new TodoItemEntityImpl(2, "Second item");
        todo2.setPriority(Priority.HIGH);
        todo2.setDeadline(DateUtils.parseDateTime("2019-01-01 16:30:00", format));
        todo2.setIsDone(true);
        mTestItems.put(todo2.getKey(), todo2);
        TodoItemEntity todo3 = new TodoItemEntityImpl(3, "Third item with a longer name");
        todo3.setPriority(Priority.LOW);
        todo3.setDeadline(DateUtils.parseDateTime("2019-01-07 17:21:00", format));
        todo3.setIsDone(true);
        mTestItems.put(todo3.getKey(), todo3);
    }

    public HashMap<Integer, TodoItemEntity> getTestItems() {
        return mTestItems;
    }

    @Override
    public List<TodoItemEntity> getAllItems() {
        List<TodoItemEntity> list = new ArrayList<>();
        Set set = mTestItems.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = (Map.Entry)iterator.next();
            TodoItemEntity item = (TodoItemEntity) entry.getValue();
            list.add(item.clone());
        }
        return list;
    }

    @Override
    public List<TodoItemEntity> getItemsByStatus(boolean isDone) {
        List<TodoItemEntity> list = new ArrayList<>();
        Set set = mTestItems.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = (Map.Entry)iterator.next();
            TodoItemEntity item = (TodoItemEntity)entry.getValue();
            if (item.getIsDone() == isDone) {
                list.add(item.clone());
            }
        }
        return list;
    }

    @Override
    public List<TodoItemEntity> getItemsByDeadline(Date deadline) {
        List<TodoItemEntity> list = new ArrayList<>();
        Set set = mTestItems.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = (Map.Entry)iterator.next();
            TodoItemEntity item = (TodoItemEntity)entry.getValue();
            if (deadline == null || (item.getDeadline() != null &&
                    item.getDeadline().getTime() <= deadline.getTime())) {
                list.add(item.clone());
            }
        }
        return list;
    }

    @Override
    public TodoItemEntity saveItem(TodoItemEntity item) {
        if (item == null) {
            throw new IllegalArgumentException("item cannot be null");
        }
        TodoItemEntity existingItem = mTestItems.get(item.getKey());
        TodoItemEntity cloned = null;
        if (existingItem == null) {
            // insert new item
            cloned = item.clone();
            cloned.setKey(mTestItems.size() + 1);
            mTestItems.put(cloned.getKey(), cloned);
        } else {
            // update existing item
            cloned = existingItem.clone();
            cloned.setDescription(item.getDescription());
            cloned.setPriority(item.getPriority());
            cloned.setDeadline(item.getDeadline() != null ? new Date(item.getDeadline().getTime()) : null);
            cloned.setIsDone(item.getIsDone());
        }
        return cloned;
    }

    @Override
    public void removeItem(int key) {
        mTestItems.remove(key);
    }
}
