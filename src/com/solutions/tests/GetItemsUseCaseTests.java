package com.solutions.tests;

import com.solutions.usecases.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class GetItemsUseCaseTests {

    private MockTodoItemRepositoryImpl mTodoItemRepository;
    private UseCase<GetItemsRequest, GetItemsResponse> mGetItemsUseCase;

    @Before
    public void setup() {
        this.mTodoItemRepository = new MockTodoItemRepositoryImpl();
        this.mGetItemsUseCase = new GetItemsUseCaseImpl(this.mTodoItemRepository);
    }

    @Test
    public void getItems() {
        final GetItemsRequest request = new GetItemsRequestImpl();
        final MockGetItemsOutputPortImpl presenter = new MockGetItemsOutputPortImpl();
        assertEquals(null, presenter.getResponse());
        final boolean result = this.mGetItemsUseCase.handle(request, presenter);
        assertEquals(presenter.getResponse().getItems().size(), this.mTodoItemRepository.getTestItems().size());
    }

    private class MockGetItemsOutputPortImpl implements OutputPort<GetItemsResponse> {

        private GetItemsResponse mResponse;

        public GetItemsResponse getResponse() {
            return mResponse;
        }

        @Override
        public void handle(GetItemsResponse response) {
            this.mResponse = response;
        }
    }
}


