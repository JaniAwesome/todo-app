package com.solutions.repositories;

import com.solutions.entities.TodoItemEntity;

import java.util.Date;
import java.util.List;

public interface TodoItemRepository {

    /**
     * Fetch all items from the repository
     *
     * @return          List of items in the repository sorted in ascending order by the deadline
     */
    List<TodoItemEntity> getAllItems();

    /**
     * Fetch all items from the repository by the status
     *
     * @param isDone    <code>true</code> return only items marked as done
     *                  <code>false</code> return only items not marked as done
     *
     * @return          List of items in the repository sorted in ascending order by the deadline
     */
    List<TodoItemEntity> getItemsByStatus(boolean isDone);

    /**
     * Fetch all items from the repository which have deadline before the given date
     *
     * @param deadline  The end date of the search
     *
     * @return          List of filtered items in the repository sorted in ascending order by the deadline
     */
    List<TodoItemEntity> getItemsByDeadline(Date deadline);

    /**
     * Save item in the repository
     *
     * @param item      Saved item. New item is appended if the item has a key (key > 0). Existing item is updated
     *                  if item does not have a key (key == 0)
     *
     * @return          Copy of the saved item
     */
    TodoItemEntity saveItem(TodoItemEntity item);

    /**
     * Remove item permanently from the repository
     *
     * @param item      Key of the item to be removed. Item is removed if it is found by the key.
     *                  Does nothing if item is not found
     */
    void removeItem(int key);
}
