package com.solutions.repositories;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.google.gson.reflect.TypeToken;
import com.solutions.entities.TodoItemEntity;

import com.google.gson.Gson;
import com.solutions.entities.TodoItemEntityImpl;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.*;

public class TodoItemRepositoryImpl implements TodoItemRepository {

    // TODO: these should be in the settings
    private static final String PATH = "Data/";
    private static final String FILE_NAME = "items.json";
    private HashMap<Integer, TodoItemEntity> mItems = new HashMap<>();

    public TodoItemRepositoryImpl() {
        loadItemsFromFile();
    }

    @Override
    public List<TodoItemEntity> getAllItems() {
        List<TodoItemEntity> list = new ArrayList<>();
        Set set = mItems.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = (Map.Entry)iterator.next();
            TodoItemEntity item = (TodoItemEntity) entry.getValue();
            list.add(item.clone());
        }
        list.sort(new Comparator<TodoItemEntity>() {
            @Override
            public int compare(TodoItemEntity a, TodoItemEntity b) {
                if (a.getDeadline() == null) {
                    return 1;
                }
                if (b.getDeadline() == null) {
                    return -1;
                }
                return (int)(a.getDeadline().getTime() - b.getDeadline().getTime());
            }
        });
        return list;
    }

    @Override
    public List<TodoItemEntity> getItemsByStatus(boolean isDone) {
        List<TodoItemEntity> list = new ArrayList<>();
        Set set = mItems.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = (Map.Entry)iterator.next();
            TodoItemEntity item = (TodoItemEntity)entry.getValue();
            if (item.getIsDone() == isDone) {
                list.add(item.clone());
            }
        }
        return list;
    }

    @Override
    public List<TodoItemEntity> getItemsByDeadline(Date deadline) {
        List<TodoItemEntity> list = new ArrayList<>();
        Set set = mItems.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = (Map.Entry)iterator.next();
            TodoItemEntity item = (TodoItemEntity)entry.getValue();
            if (deadline == null || (item.getDeadline() != null &&
                    item.getDeadline().getTime() <= deadline.getTime())) {
                list.add(item.clone());
            }
        }
        return list;
    }

    @Override
    public TodoItemEntity saveItem(TodoItemEntity item) {
        if (item == null) {
            throw new IllegalArgumentException("item cannot be null");
        }
        TodoItemEntity existingItem = mItems.get(item.getKey());
        TodoItemEntity cloned = null;
        if (existingItem == null) {
            // insert new item
            cloned = item.clone();
            cloned.setKey(mItems.size() + 1);
        } else {
            // update existing item
            cloned = existingItem.clone();
            cloned.setDescription(item.getDescription());
            cloned.setPriority(item.getPriority());
            cloned.setDeadline(item.getDeadline() != null ? new Date(item.getDeadline().getTime()) : null);
            cloned.setIsDone(item.getIsDone());
        }
        mItems.put(cloned.getKey(), cloned);
        try {
            storeItemsToFile();
        } catch (IOException e) {
            // TODO: handle exception
        }
        return cloned;
    }

    @Override
    public void removeItem(int key) {
        mItems.remove(key);
        try {
            storeItemsToFile();
        } catch (IOException e) {
            // TODO: handle exception
        }
    }

    private void loadItemsFromFile() {
        // TODO: refactor this method
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        gsonBuilder.registerTypeAdapter(
                TodoItemEntity.class,
                new TodoItemEntityInstanceCreator(0, "")
        );
        Gson gson = gsonBuilder.create();
        Type listType = new TypeToken<List<TodoItemEntityImpl>>() {}.getType();
        String json = "";
        try(FileReader fileReader = new FileReader(PATH + FILE_NAME)) {
            int i;
            while ((i = fileReader.read()) != -1) {
                char ch = (char) i;
                json = json + ch;
            }
        } catch (IOException e) {
            // TODO: handle exception
            return;
        }
        if (json.length() == 0) {
            return;
        }
        List<TodoItemEntity> list = gson.fromJson(json, listType);
        mItems.clear();
        for (TodoItemEntity entity : list) {
            mItems.put(entity.getKey(), entity);
        }
    }

    private void storeItemsToFile() throws IOException {
        // TODO: refactor this method
        File directory = new File(PATH);
        if (!directory.exists()) {
            if (!directory.mkdir()) {
                throw new IOException("Could not create data folder");
            }
        }
        Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        Set set = mItems.entrySet();
        Iterator iterator = set.iterator();
        List<TodoItemEntity> list = new ArrayList<>();
        while(iterator.hasNext()) {
            Map.Entry entry = (Map.Entry)iterator.next();
            TodoItemEntity item = (TodoItemEntity) entry.getValue();
            list.add(item);
        }
        String json = gson.toJson(list);
        try (FileWriter file = new FileWriter(PATH + FILE_NAME)) {
            file.write(json);
        } catch (IOException e) {
            // TODO: handle exception
        }
    }

    private class TodoItemEntityInstanceCreator implements InstanceCreator<TodoItemEntity> {

        private int mKey;
        private String mDescription;

        public TodoItemEntityInstanceCreator(int key, String description) {
            mKey = key;
            mDescription = description;
        }

        @Override
        public TodoItemEntity createInstance(Type type) {
            TodoItemEntity item = new TodoItemEntityImpl(mKey, mDescription);

            // return it to gson for further usage
            return item;
        }
    }
}
