package com.solutions;

import com.solutions.repositories.TodoItemRepository;
import com.solutions.repositories.TodoItemRepositoryImpl;
import com.solutions.ui.State;
import com.solutions.ui.TodoListUI;
import com.solutions.ui.TodoListUIConsoleImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
        // create repository
        TodoItemRepository repository = new TodoItemRepositoryImpl();
        // create localization
        Localization localization = new LocalizationImpl(Constants.LANG_CODE_FI);
        // create service
        TodoListService service = new TodoListServiceImpl(repository, localization);
        // create UI (console)
        TodoListUI ui = new TodoListUIConsoleImpl(service, localization);
        // loop user input - render cycle until EXIT has been chosen
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (ui.getState() != State.EXIT) {
            ui.render();
            String input = null;
            try {
                input = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ui.handleInput(input);
        }
    }
}
