package com.solutions;

import java.text.SimpleDateFormat;
import java.util.HashMap;

public class LocalizationImpl implements Localization {

    private String mLangCode = "en-US";
    private HashMap<String, String> mStringsFi = new HashMap<>();
    private HashMap<String, String> mStringsEn = new HashMap<>();

    public  LocalizationImpl(String langCode) {
        // TODO: get strings from file
        if (langCode != null &&
                (Constants.LANG_CODE_FI.equals(langCode) || Constants.LANG_CODE_EN.equals(langCode))) {
            mLangCode = langCode;
        }
        mStringsFi.put("app_name", "TODO-sovellus");
        mStringsFi.put("index", "Nro");
        mStringsFi.put("description", "Kuvaus");
        mStringsFi.put("deadline", "Aika");
        mStringsFi.put("priority", "Prioriteetti");
        mStringsFi.put("done", "Tehty");
        mStringsFi.put("priority_low", "Matala");
        mStringsFi.put("priority_high", "Korkea");
        mStringsFi.put("action_select_item", "[%d - %d] = Valitse tehtävä");
        mStringsFi.put("action_add_item", "[a] = Lisää tehtävä");
        mStringsFi.put("action_exit", "[e] = Lopeta");
        mStringsFi.put("action_edit", "[e] = Muokkaa");
        mStringsFi.put("action_remove", "[r] = Poista");
        mStringsFi.put("action_mark_done", "[m] = Merkkaa tehdyksi");
        mStringsFi.put("action_cancel", "[c] = Peruuta");
        mStringsFi.put("input_description", "Syötä kuvaus");
        mStringsFi.put("input_deadline", "Syötä aika (pp.kk.vvvv)");
        mStringsFi.put("input_priority", "Syötä prioriteetti [1 = matala, 2 = korkea]");
        mStringsFi.put("invalid_input", "Virheellinen syöttö");
        mStringsFi.put("item_removed", "Tehtävä poistettu");
        mStringsFi.put("item_marked_done", "Tehtävä merkattu tehdyksi");
        mStringsFi.put("item_saved", "Tehtävä tallennettu");
        mStringsFi.put("selected_item", "Valittu tehtävä:");
        mStringsFi.put("new_item", "Lisää uusi tehtävä:");
        mStringsFi.put("input", "Syöttö:");

        mStringsEn.put("app_name", "TODO app");
        mStringsEn.put("index", "Nbr");
        mStringsEn.put("description", "Description");
        mStringsEn.put("deadline", "Deadline");
        mStringsEn.put("priority", "Priority");
        mStringsEn.put("done", "Done");
        mStringsEn.put("priority_low", "Low");
        mStringsEn.put("priority_high", "High");
        mStringsEn.put("action_select_item", "[%d - %d] = Select item");
        mStringsEn.put("action_add_item", "[a] = Add item");
        mStringsEn.put("action_exit", "[e] = Exit");
        mStringsEn.put("action_edit", "[e] = Edit");
        mStringsEn.put("action_remove", "[r] = Remove");
        mStringsEn.put("action_mark_done", "[m] = mark done");
        mStringsEn.put("action_cancel", "[c] = Cancel");
        mStringsEn.put("input_description", "input description");
        mStringsEn.put("input_deadline", "Input deadline (yyyy.MM.dd)");
        mStringsEn.put("input_priority", "Input priority [1 = low, 2 = high]");
        mStringsEn.put("invalid_input", "Invalid input");
        mStringsEn.put("item_removed", "Item removed");
        mStringsEn.put("item_marked_done", "Item marked done");
        mStringsEn.put("item_saved", "Item saved");
        mStringsEn.put("selected_item", "Selected item:");
        mStringsEn.put("new_item", "Add new Item:");
        mStringsEn.put("input", "Input:");
    }

    @Override
    public SimpleDateFormat getDateFormat() {
        switch (mLangCode) {
            case Constants.LANG_CODE_FI:
                return new SimpleDateFormat("dd.MM.yyyy");
            default:
                return new SimpleDateFormat("yyyy-MM-dd");
        }
    }

    @Override
    public String getString(String key) {
        String str;
        switch (mLangCode) {
            case Constants.LANG_CODE_FI:
                str = mStringsFi.get(key);
                if (str == null) {
                    str = mStringsEn.get(key);
                }
                break;
            default:
                str = mStringsEn.get(key);
        }
        return str != null ? str : "";
    }
}
