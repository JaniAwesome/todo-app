package com.solutions;

public abstract class Constants {

    public static final String LANG_CODE_EN = "en-US";

    public static final String LANG_CODE_FI = "fi-FI";
}
