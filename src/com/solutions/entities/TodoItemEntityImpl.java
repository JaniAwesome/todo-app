package com.solutions.entities;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.Objects;

/*
* One basic implementation of a TodoItemEntity
 */
public class TodoItemEntityImpl implements TodoItemEntity {

    @SerializedName("key")
    private int mKey;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("priority")
    private Priority mPriority = Priority.LOW;

    @SerializedName("deadline")
    private Date mDeadline = null;

    @SerializedName("isDone")
    private boolean mIsDone = false;

    @SerializedName("created")
    private Date mCreated;

    public TodoItemEntityImpl(int key, String description) {
        mKey = key;
        mDescription = description;
        mCreated = new Date();
    }

    @Override
    public void setKey(int key) {
        mKey = key;
    }

    @Override
    public int getKey() {
        return mKey;
    }

    @Override
    public void setDescription(String description) {
        mDescription = description;
    }

    @Override
    public String getDescription() {
        return mDescription;
    }

    @Override
    public void setPriority(Priority priority) {
        mPriority = priority;
    }

    @Override
    public Priority getPriority() {
        return mPriority;
    }

    @Override
    public void setDeadline(Date date) {
        mDeadline = date;
    }

    @Override
    public Date getDeadline() {
        return mDeadline;
    }

    @Override
    public void setIsDone(boolean isDone) {
        mIsDone = isDone;
    }

    @Override
    public boolean getIsDone() {
        return mIsDone;
    }

    @Override
    public void setCreated(Date created) {
        mCreated = created;
    }

    @Override
    public Date getCreated() {
        return mCreated;
    }

    @Override
    public String toJSON() {
        // TODO: implement toJson
        return null;
    }

    @Override
    public void fromJSON() {
        // TODO: implement fromJson
    }

    @Override
    public TodoItemEntity clone() {
        TodoItemEntity clone = new TodoItemEntityImpl(mKey, mDescription);
        clone.setPriority(mPriority);
        clone.setDeadline(mDeadline != null ? new Date(mDeadline.getTime()) : null);
        clone.setIsDone(mIsDone);
        clone.setCreated(mCreated != null ? new Date(mCreated.getTime()) : null);
        return clone;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof TodoItemEntityImpl)) {
            return false;
        }
        TodoItemEntityImpl item = (TodoItemEntityImpl) o;
        return item.getKey() == mKey &&
               Objects.equals(item.getDescription(), mDescription) &&
               Objects.equals(item.getPriority(),mPriority) &&
               Objects.equals(item.getDeadline(),mDeadline) &&
               item.getIsDone() == mIsDone &&
                Objects.equals(item.getCreated(), mCreated);
    }

    public int hashCode() {
        return mKey;
    }
}
