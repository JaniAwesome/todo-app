package com.solutions.entities;

import java.util.Date;

public interface TodoItemEntity extends ItemEntity{

    void setPriority(Priority priority);

    Priority getPriority();

    void setDeadline(Date date);

    Date getDeadline();

    void setIsDone(boolean isDone);

    boolean getIsDone();

    void setCreated(Date created);

    Date getCreated();

    TodoItemEntity clone();
}
