package com.solutions.entities;

/**
 * Each item instance must have a key and description so it is distinctive and
 * descriptive.
 * Instances should implement serialization and deserialization abilities
 */
public interface ItemEntity {

    void setKey(int key);

    int getKey();

    void setDescription(String description);

    String getDescription();

    /**
     * Serialize item to JSON string
     *
     * @return          Item as JSON string
     */
    String toJSON();

    /**
     * Parse item from JSON string
     */
    void fromJSON();
}
