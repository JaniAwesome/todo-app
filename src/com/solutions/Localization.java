package com.solutions;

import java.text.SimpleDateFormat;

public interface Localization {

    SimpleDateFormat getDateFormat();

    String getString(String key);
}
