package com.solutions.utils;

public class IntegerUtils {

    public static Integer tryParseInt(String value) {
        try {
            Integer i = Integer.parseInt(value);
            return i;
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
